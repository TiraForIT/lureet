﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContactsApp.Models
{
    public class AppClaim
    {
        [Key]
        public int ClaimId { get; set; }
        public string ClaimName { get; set; }
    }
}
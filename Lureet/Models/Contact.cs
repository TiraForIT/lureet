﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContactsApp.Models
{

    public enum Status
    {
        Active = 1,
        Deleted = 2
    }
    public enum Location
    {
        Internal = 1,
        External = 2
    }
    public enum Level
    {
        // TODO: I want to Dispaly text to be show every where instead of the index
        [Display(Name = "روضة أطفال 1")]
        KG1 = -1,
        [Display(Name = "روضة أطفال 2")]
        KG2 = -2,
        [Display(Name = "الصف الاول")]
        First = 1,
        [Display(Name = "الصف الثاني")]
        Second = 2,
        [Display(Name = "الصف الثالث")]
        Third = 3,
        [Display(Name = "الصف الرابع")]
        Fourth = 4,
        [Display(Name = "الصف الخامس")]
        Fifth = 5,
        [Display(Name = "الصف السادس")]
        Sixth = 6,
        [Display(Name = "الصف السابع")]
        Seventh = 7,
        [Display(Name = "الصف الثامن")]
        Eighth = 8,
        [Display(Name = "الصف التاسع")]
        Ninth = 9,
        [Display(Name = "الصف العاشر")]
        Tenth = 10,

    }
    public enum Gender
    {
        // TODO: I want to Dispaly text to be show every where instead of the index

        [Display(Name = "ذكر")]
        Male = 1,
        [Display(Name = "انثى")]
        Female = 2
    }

    public class Contact
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required(ErrorMessage ="الرجاء ادخال الاسم الكامل")]
        [StringLength(200)]
        [DisplayName("الاسم الكامل")]
        public string FullName { get; set; }

        //[StringLength(200)]
        //[DisplayName("الاسم الثاني")]
        //public string SecondName { get; set; }

        //[StringLength(200)]
        //[DisplayName("Third Name")]
        //public string ThirdName { get; set; }

        //[Required]
        //[StringLength(200)]
        //[DisplayName("Last Name")]
        //public string LastName { get; set; }

        [Required(ErrorMessage ="الرجاء اختيار الجنس")]
        [DisplayName("الجنس")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Gender Gender { get; set; }

        [Required(ErrorMessage = "الرجاء اختيار الصف الدراسي")]
        [DisplayName("الصف")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Level level { get; set; }

        // TODO : How to change the datepicker to better one
        [DisplayName("تاريخ الالتحاق بالمدرسة")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? AttendanceDate { get; set; }

        //TODO: How to diaplay this as "مشترك" if true or "غير مشترك" if false 
        [DisplayName("إشتراك الباص")]
        public bool Transportation { get; set; }

        [StringLength(500)]
        [DisplayName("عنوان السكن")]
        public string Address { get; set; }


        [StringLength(100)]
        [DisplayName("هاتف الاب")]
        [DataType(DataType.PhoneNumber)]
        public string FatherPhone { get; set; }

        [StringLength(100)]
        [DisplayName("هاتف الام")]
        [DataType(DataType.PhoneNumber)]
        public string MotherPhone { get; set; }

        [StringLength(200)]
        [DisplayName("رقم احد الاقارب")]
        [DataType(DataType.PhoneNumber)]
        public string RelativeMobile { get; set; }

        [StringLength(200)]
        [DisplayName("صلة القرابة")]
        public string Relastionship { get; set; }

        [EmailAddress(ErrorMessage = "البريد الالكتروني غير صحيح")]
        [DataType(DataType.EmailAddress)]
        [MaxLength(100)]
        [DisplayName("البريد الالكتروني")]
        public string EmailAddress { get; set; }


        [StringLength(500)]
        [DisplayName("ملاحظات")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }


        [DisplayName("Status")]
        public Status Status { get; set; }

        public bool AddedBySystem { get; set; }


        [DisplayName("تاريخ الانشاء")]
        [DataType(DataType.DateTime)]
        public DateTime? CreatedDate { get; set; }

        [DisplayName("اخر تاريخ للتعديل")]
        [DataType(DataType.DateTime)]
        public DateTime? LastModifiedDate { get; set; }

        public virtual List<ActionLog> Logs { get; set; }
    }
}
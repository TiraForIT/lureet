﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ContactsApp.Models
{
    public class ActionLog
    {
        [Key]
        public int Id { get; set; }

        public string Action { get; set; }

        
        public string UserId { get; set; }

        public DateTime ActionTime { get; set; }

        
        public int ContactId { get; set; }

        [ForeignKey("ContactId")]
        public virtual Contact Contact { internal get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User {  get; set; }
    }
}
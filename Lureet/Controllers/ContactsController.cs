﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContactsApp.Models;
using Microsoft.AspNet.Identity;
using ContactsApp.Filters;

namespace ContactsApp.Controllers
{
    [Authorize]
    [MessageFilter]
    public class ContactsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        // GET: Contacts
        public ActionResult Index()
        {
          
            return View(new List<Contact>());

        }

        public JsonResult GetJson(Status status = Status.Active)
        {
            var contacts = db.Contacts.Where(a => a.Status == status).ToList();
            return Json(new { aaData = contacts }, JsonRequestBehavior.AllowGet);
        }

        // GET: Contacts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // GET: Contacts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Contacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FullName,Gender,Level,AttendanceDate,Transportation,Address,FatherPhone,MotherPhone,RelativeMobile,Relastionship,EmailAddress,Note")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                DateTime curDate = DateTime.Now;
                contact.Status = Status.Active;
                contact.Logs = new List<ActionLog>();
                contact.Logs.Add(new ActionLog
                {
                    Action = "Created",
                    UserId = User.Identity.GetUserId(),
                    ActionTime = curDate
                });

                contact.CreatedDate = curDate;
                contact.LastModifiedDate = curDate;
                db.Contacts.Add(contact);
                db.SaveChanges();
                return RedirectToAction("Index", new { msg = "Contact successfully added." });
            }

            return View(contact);
        }

        // GET: Contacts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: Contacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FullName,Gender,Level,AttendanceDate,Transportation,Address,FatherPhone,MotherPhone,RelativeMobile,Relastionship,EmailAddress,Status,Note")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                DateTime curDate = DateTime.Now;
                db.ActionLogs.Add(new ActionLog
                {
                    Action = "Updated",
                    UserId = User.Identity.GetUserId(),
                    ActionTime = curDate,
                    ContactId = contact.ID
                });
                contact.CreatedDate = curDate;
                contact.LastModifiedDate = curDate;
                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { msg = "Contact successfully updated." });
            }
            return View(contact);
        }

        // GET: Contacts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: Contacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contact contact = db.Contacts.Find(id);
            DateTime curDate = DateTime.Now;
            db.ActionLogs.Add(new ActionLog
            {
                Action = "Deleted",
                UserId = User.Identity.GetUserId(),
                ActionTime = curDate,
                ContactId = contact.ID
            });
            contact.CreatedDate = curDate;
            contact.LastModifiedDate = curDate;
            contact.Status = Status.Deleted;
            db.Entry(contact).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index", new { msg = "Contact successfully deleted." });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

namespace ContactsApp.Migrations
{
    using ContactsApp.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ContactsApp.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ContactsApp.Models.ApplicationDbContext context)
        {
            //This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.E.g.

            context.Claims.AddOrUpdate(
              p => p.ClaimName,
              new Models.AppClaim { ClaimName = "View Contacts" },
              new Models.AppClaim { ClaimName = "Manage Contacts" },
              new Models.AppClaim { ClaimName = "View Contacts History" },
              new Models.AppClaim { ClaimName = "Approve Users" }
            );

            context.Roles.AddOrUpdate(
               p => p.Name,
                new Microsoft.AspNet.Identity.EntityFramework.IdentityRole { Name = "Admin" },
                new Microsoft.AspNet.Identity.EntityFramework.IdentityRole { Name = "User" },
                 new Microsoft.AspNet.Identity.EntityFramework.IdentityRole { Name = "Employee" },
                  new Microsoft.AspNet.Identity.EntityFramework.IdentityRole { Name = "Manager" }
             );

            string defaultUser = "AhmedAlkaff@outlook.com";
            //if (!System.Diagnostics.Debugger.IsAttached)
            //    System.Diagnostics.Debugger.Launch();
            if (!context.Users.Any(u => u.UserName == defaultUser))
            {
                try
                {

                    var store = new UserStore<ApplicationUser>(context);
                    var manager = new UserManager<ApplicationUser>(store);
                    var user = new ApplicationUser
                    {
                        FirstName = "Ahmed",
                        LastName = "Alkaff",
                        EmailConfirmed = true,
                        Email = defaultUser,
                        UserName = defaultUser,
                        ApprovedByAdmin = true
                    };

                    foreach (var item in context.Claims.ToList())
                    {
                        user.Claims.Add(new IdentityUserClaim
                        {
                            ClaimType = item.ClaimName,
                            ClaimValue = item.ClaimId.ToString(),
                            UserId = user.Id
                        });
                    }

                    manager.Create(user, "Alk@ff2019");
                    manager.AddToRole(user.Id, "Super Admin");

                }
                catch (Exception ex)
                {
                    if (!System.Diagnostics.Debugger.IsAttached)
                    {
                        System.Diagnostics.Debugger.Launch();
                    }

                    throw ex;
                }


            }
        }
    }
}

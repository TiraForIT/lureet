﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContactsApp.Filters
{
    public class MessageFilter: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.RequestContext.HttpContext.Request.Params["msg"] != null)
            {
                filterContext.Controller.ViewBag.Message = Convert.ToString(filterContext.RequestContext.HttpContext.Request.Params["msg"]);
            }
        }
        
    }
}